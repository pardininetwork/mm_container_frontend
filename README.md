# Micromedia Sites

## Core Product
Small sites for businesses that are 5 pages or less with a CMS to manage the content that take 30 mintues to set up. 
Includes 5 default templates, and the ability to create custom templates with React.
Each default template has the following pages:

- Home
- Menu / Services
- About
- Contact / Locations
- Blog / Articles


Micromedia sites tie into small / local business API's. 
It also includes a headless Wordpress integration. 
API integrations include:

- Google sites
- Google ads
- Menu platforms
- Reservation sites
- uber eats, delivery services
- SEO dashboards
- Google Analytics
- Square
- Stripe
- Other POS systems


CMS

- Select template
- Manage each page content
- Manage account settings
- Manage API integrations
- Add a post / article
- Contact Micromedia for web dev


Micromedia will also offer custom web dev to any customer.

Micromedia sites charges per year ($25 ?)


## Tracking
https://paralux.atlassian.net/jira/software/projects/MMS/boards/2/roadmap


# Subroot.io

Offers a dashboard for managing your Micromedia Sites (sellable to web dev shops, marketing agencies, advertising agency, brand managers, and SEO experts)

## Tracking
https://paralux.atlassian.net/jira/software/projects/SUB/boards/3/roadmap